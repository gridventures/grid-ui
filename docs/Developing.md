---
sidebar: auto
---
<style>
ol {
  list-style: decimal;
}
</style>

# @gridventures/grid-ui

## Before you start

**We are using node-version 11.12.0**

```bash
$ npm i vuepress -g
```

To start the local docs:
```bash
$ vuepress dev docs
```
You can access the docs at [`localhost:8081`](http://localhost:8081).

> Please do not use a package json script to start the vuepress dev server, because it will crash then because of the project configurations.


> We will develop the components in a local vue app and not in vuepress. For that:
```bash
$ npm run serve
```
You can access the app at [`localhost:8080`](http://localhost:8080).

## Important files

### .npmignore
All files which aren't required to run the vue plugin should be implemented here, so package size is the lowest as possible.

### index.js
The file which is imported by the user to initialize the vue plugin

### styles.scss
The stylesheet which is imported by the user to get the grid-ui custom classes

### docs/.vuepress/config.js
Vuepress configuration

## Branching Guide
The repository is based on Git-Flow. Please work in feature-branches so every feature can be merged everytime into develop.

Git Flow Configuration
```
Production Branch: master
Develop Branch: develop
Feature Branch Prefix: feature/
Hotfix Branch Prefix: hotfix/
Release Branch Prefix: release/
Version Tag Prefix: v
```

### Start your feature
1. Start your feature in `feature/*`.
2. Create your component in `src/components/*`.
3. Import component in `/index.js` and register it like the other components.
4. Create a demo view in `src/views/*` and import it in `src/router.js`. After this a new link will appear in the sidebar in your local app (`localhost:8080`).
5. Add your component in different variants to the view for testing purposes.
6. You are ready? Then [**document**](#documentation-guide) your code.

### Feature is ready to be merged
1. Test the docs locally by `$ vuepress build docs`
2. When you are ready push the feature.
3. The feature-staging is build to `https://gridui.gridventures.de/feature/{{your-feature-name}}`.
4. After this is done you can [create a Pull Request here](https://bitbucket.org/gridventures/grid-ui/pull-requests/). (!!!Pull Request to `develop`)
5. Please add the staging link to the pull request.
6. If the pull request is approved the feature will be merged into `develop`.
7. Please remove your feature branch locally after pull-request was approved.

## Documentation Guide

### Document your component
First of all you have to add a `name` property to your component:
```js
export default {
  name: 'YourComponent',
  ...
};
```

#### Prop Documentation
> **NOTE**: Not every prop needs a description. Props like `required`, `disabled` and similiar are mostly self-descriptive.

```js
props: {
  // description of the prop
  someProp: {
    // Custom Type Description
    type: String,
    // Custom Default description
    default: () => nanoid(),
  },
},
```

#### Slot Documentation
```vue
<!-- Slot description -->
<slot name="header">
  <!-- Default slot content description -->
  <th>title</th>
</slot>
```

#### Event Documentation
```js
methods: {
  clear () {
    // Event description
    // @arg Description of the argument which is passed, here: true
    this.$emit('onclear', true)
  }
}
```

#### Methods Documentation
Methods documentation is optional and not done automatically by vuese. So you have to initialize vuese by adding `@vuese` as first comment before the method.
```js
methods: {
  // @vuese
  // Description of the method
  // @arg Argument which can be passed to the method
  clear (bol) {
    // ...
  }
}
```
> For more have a look into [vuese documentation](https://vuese.org/cli/#writing-documentation-for-your-component).

### Create Markdown File for component
1. After you have documented your component you can create a markdown file for your component in `docs/*`. The name should be lowercased.
2. The document should have following basic structure: 
```md
# {{ Component Name }} <Badge version="{{ will be filled by release }}" update-version="none" />

## Examples
// All Examples as vue components and with code example here

## Documentation
<[vuese](@/src/components/{{YourComponent}}.vue)
```
> You can split your examples with `###` Headlines if its needed.

## Complex Examples
For more complex examples you can create a vue component therefor in `docs/.vuepress/components/*`.

## Release Guide
> Coming Soon