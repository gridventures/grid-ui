---
sidebar: auto
---

<style>
ul {
    list-style: disc;
}
</style>

# Changelog

## v0.1.1-1
### Updated Components
- Sidebar
    - fix wrong custom validation for prop `position`

## v0.1.1
- fixed theming documentation

### New Components
- Sidebar / SidebarItem

### Updated Configurations
- add `background` to palette generator
- use `background` color for body background

## v0.1.0 :tada:
First real release :tada:

### New Configurations
- new `gridUiDefaults` utility for `tailwind.config.js`
### Updated Configurations
- updated `createTailwindColorSet`

## v0.0.5
### New Components
- Checkbox
- Datepicker
- Input
- Radio
- Select
- Switch
- Textarea
### Updated Components
- Button:
    - Implemented `rounded` prop
### New Configurations
- `prefix`
    - Change the prefix of the components within the plugin options (default: `g`)
