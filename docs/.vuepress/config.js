const process = require('process');
const webpack = require('webpack');

module.exports = {
  title: 'Grid UI',
  dest: 'dist',
  port: 8081,
  description: 'Flexible ui-framework for vue.js powered by tailwindcss and built by Gridventures GmbH',
  theme: '@vuepress/theme-default',
  themeConfig: {
    nav: [
      { text: 'Guide', link: '/guide/' },
      { text: 'Theming Guide', link: '/theming' },
      { text: 'Components', link: '/component/' },
      { text: 'Roadmap', link: '/roadmap' },
      { text: 'Changelog', link: '/changelog' },
    ],
    sidebar: {
      '/component/': [
        ['button', 'Button'],
        ['dropdown', 'Dropdown'],
        {
          title: 'Form',
          children: [
            'form/autocomplete',
            'form/checkbox',
            'form/datepicker',
            'form/input',
            'form/radio',
            'form/select',
            'form/switch',
            'form/textarea',
          ],
        },
        {
          title: 'Navigation',
          children: [
            'navigation/sidebar',
          ],
        },
      ],
    },
  },

  postcss: {
    plugins: [require('tailwindcss')('./tailwind.config.js'), require('autoprefixer')],
  },

  markdown: {
    extendMarkdown: (md) => {
      md.use(require('markdown-it-vuese'), { /* options */ })
    },
  },

  configureWebpack: (config, isServer) => {
    return { plugins: [
      new webpack.EnvironmentPlugin({ ...process.env })
    ]};
  }
};
