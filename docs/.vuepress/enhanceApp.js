import '../../src/styles.scss';
import 'typeface-inter/inter.css';

const isServer = !(typeof window != 'undefined' && window.document);

export default ({
  Vue, // the version of Vue being used in the VuePress app
  options, // the options for the root Vue instance
  router, // the router instance for the app
  siteData // site metadata
}) => {
  if (!isServer) {
    import('../../index').then((GridUi) => {
      Vue.use(GridUi.default);
    });
    import('vue-analytics').then((VueAnalytics) => {
      Vue.use(VueAnalytics, {
        id: 'UA-108532436-3',
      });
    });
  }
}
