# Switch <Badges version="0.0.5" update-version="none" />

## Examples
<g-switch>Label <strong>HTML</strong></g-switch>
<g-switch label="Label" />
<g-switch label="Label" disabled />

```vue
<g-switch>Label <strong>HTML</strong></g-switch>
<g-switch label="Label" />
<g-switch label="Label" disabled />
```

## Documentation
<[vuese](@/src/components/Form/Switch.vue)