# Autocomplete <Badges version="0.0.5" update-version="none" />

## Examples

### Array of strings using Placeholder
Enter for example 'a'.

<autocomplete-examples :option="1" />

```vue
<g-autocomplete
  v-model="selectedString"
  placeholder="Placeholder"
  autofocus
  :data="['Max', 'Shannen', 'Vlad', 'Hakeem', 'Zoe', 'Franco']"
/>
```

### Array of objects with preselect first result
Enter for example 'v'.

<autocomplete-examples :option="2" />

```vue
<g-autocomplete
  v-model="selected"
  label="Autocomplete"
  :data="[
      { name: 'Vlad Dyer', age: 20 },
      { name: 'Shannen Velez', age: 28 },
      { name: 'Ellen Collier', age: 31 },
  ]"
  field="name"
  preselect
/>
```

### Array of objects with deep filtering
Enter for example 'v'.

<autocomplete-examples :option="3" />

```vue
<g-autocomplete
  v-model="selected"
  label="Autocomplete"
  :data="[
      { name: { fullName: 'Vlad Dyer' }, age: 20 },
      { name: { fullName: 'Shannen Velez' }, age: 28 },
      { name: { fullName: 'Ellen Collier' }, age: 31 },
  ]"
  field="name.fullName"
/>
```

### With Create Button

::: tip
You can use the current user input value by using the variable `%N%` in the `create-option-text`.
:::

<autocomplete-examples :option="4" />

```vue
<g-autocomplete
  v-model="selected"
  label="Autocomplete"
  :data="[
      { name: 'Vlad Dyer', age: 20 },
      { name: 'Shannen Velez', age: 28 },
      { name: 'Ellen Collier', age: 31 },
  ]"
  field="name"
  create-option
  create-option-text="Create new user: %N%"
  @createClick="(e) => createClicked = e"
/>
```

## Documentation
<[vuese](@/src/components/Form/Autocomplete.vue)