# Datepicker <Badges version="0.0.5" update-version="none" />

## Examples

 <datepicker-examples />

```vue
<g-datepicker
  v-model="date"
  label="Datepicker"
  required
/>
<g-datepicker
  v-model="customDate"
  label="Datepicker with custom format and buttons"
  date-format="DD.MM.YYYY"
  today-button
/>
<g-datepicker
  v-model="customDate"
  label="Datepicker disabled"
  date-format="DD.MM.YYYY"
  disabled
/>
```


## Documentation
<[vuese](@/src/components/Form/Datepicker.vue)