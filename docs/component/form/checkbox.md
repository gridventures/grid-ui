# Checkbox <Badges version="0.0.5" update-version="none" />

## Examples
### With Boolean value
<g-checkbox required>Checkbox with <i>HTML Label</i> Required</g-checkbox>
<g-checkbox
    label="Not required"
/>
<g-checkbox
    :value="true"
    label="Checkbox disabled"
    disabled
/>

```vue
<g-checkbox
  v-model="checkbox"
  required
>
  Checkbox with <i>HTML Label</i> Required
</g-checkbox>
<g-checkbox
  v-model="checkbox"
  label="Not required"
/>
<g-checkbox
  v-model="checkbox"
  label="Checkbox disabled"
  disabled
/>
```

### As checkbox group
<checkbox-group-example />

```js
data: {
  check: [],
},
```
```vue
<g-checkbox
  v-model="check"
  name="checkbox"
  native-value="Tim"
  label="Tim"
/>
<g-checkbox
  v-model="check"
  name="checkbox"
  native-value="Mike"
  label="Mike"
/>
<g-checkbox
  v-model="check"
  name="checkbox"
  native-value="Bernd"
  label="Bernd"
/>
```

## Documentation
<[vuese](@/src/components/Form/Checkbox.vue)