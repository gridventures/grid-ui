# Radio <Badges version="0.0.5" update-version="none" />

## Examples
<g-radio
    name="test"
    native-value="Tim"
    label="Tim"
/>
<g-radio
    name="test"
    native-value="Bernd"
    label="Bernd"
    disabled
/>
<g-radio
    name="test"
    native-value="Lars"
    label="Lars"
/>

```vue
<g-radio
  v-model="radio"
  native-value="Tim"
  label="Tim"
/>
<g-radio
  v-model="radio"
  native-value="Bernd"
  label="Bernd"
  disabled
/>
<g-radio
  v-model="radio"
  native-value="Lars"
  label="Lars"
/>
```

## Documentation
<[vuese](@/src/components/Form/Radio.vue)