# Sidebar <Badges version="0.1.1" update-version="0.1.1-1" />

## Examples

### Mini Variant
<g-sidebar position="relative" :mini="true">
  <g-sidebar-item el="button" :mini="true" icon="home">Home</g-sidebar-item>
  <g-sidebar-item el="button" :mini="true" icon="warehouse">Warehouse</g-sidebar-item>
  <template slot="bottom">
    <g-sidebar-item el="button" bg="bg-alert" :mini="true" icon="power">Logout</g-sidebar-item>
  </template>
</g-sidebar>

```vue
<g-sidebar :mini="true">
  <g-sidebar-item to="/" :mini="true" icon="home">Home</g-sidebar-item>
  <g-sidebar-item to="/warehouse" :mini="true" icon="warehouse">Warehouse</g-sidebar-item>
  <template slot="bottom">
    <g-sidebar-item to="/logout" bg="bg-alert" :mini="true" icon="power">Logout</g-sidebar-item>
  </template>
</g-sidebar>
```

### Normal Dark Variant
<g-sidebar position="relative" dark>
  <g-sidebar-item el="button" dark icon="home">Home</g-sidebar-item>
  <g-sidebar-item el="button" dark icon="warehouse">Warehouse</g-sidebar-item>
  <template slot="bottom">
    <g-sidebar-item el="button" bg="bg-alert" dark icon="power">Logout</g-sidebar-item>
  </template>
</g-sidebar>

```vue
<g-sidebar dark>
  <g-sidebar-item to="/" dark icon="home">Home</g-sidebar-item>
  <g-sidebar-item to="/warehouse" dark icon="warehouse">Warehouse</g-sidebar-item>
  <template slot="bottom">
    <g-sidebar-item to="/logout" bg="bg-alert" dark icon="power">Logout</g-sidebar-item>
  </template>
</g-sidebar>
```

### Documentation
<[vuese](@/src/components/Navigation/Sidebar.vue)
<[vuese](@/src/components/Navigation/SidebarItem.vue)