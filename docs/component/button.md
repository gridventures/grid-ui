# Buttons <Badges version="0.0.5" update-version="none" />

## Examples
### Coloring
<div
  v-for="v in ['Primary', 'Secondary', 'Light', 'Dark', 'Neutral', 'Alert', 'Warning', 'Success', 'Transparent']"
  :key="v"
  class="inline-flex mr-2 mb-2"
>
  <g-button
    :color="v.toLowerCase()"
  >
    {{ v }}
  </g-button>
</div>

```vue
<g-button color="primary">Button</g-button>
```

### Sizing
<div class="mr-2 inline-flex">
  <g-button color="primary" large>Large Button</g-button>
</div>

```vue
<g-button color="primary" large>Large Button</g-button>
```

### Border Radius Variants
<div class="mr-2 inline-flex">
  <g-button color="secondary" rounded="rounded-full">Full Border Radius</g-button>
</div>
<div class="mr-2 inline-flex">
  <g-button color="dark" rounded="rounded-none">No Border Radius</g-button>
</div>

```vue
<g-button
  color="secondary"
  rounded="rounded-full"
>
  Full Border Radius
</g-button>
<g-button color="dark" rounded="rounded-none">No Border Radius</g-button>
```

### Icon Buttons

::: tip
We are using [materialdesignicons](https://materialdesignicons.com/).
:::

<div>
  <g-button color="primary" icon="home">Icon plus Text</g-button>
</div>
<div class="mt-2 mr-2 inline-flex">
  <g-button color="primary" icon="home" />
</div>
<div class="mt-2 mr-2 inline-flex">
  <g-button color="primary" icon="home" rounded="rounded-full" />
</div>
<div class="mt-2 mr-2 inline-flex">
  <g-button
    color="primary"
    icon="home"
    rounded="rounded-full"
    large
  />
</div>

```vue
<g-button color="primary" icon="home">Icon plus Text</g-button>
<g-button color="primary" icon="home" />
<g-button color="primary" icon="home" rounded="rounded-full" />
```

### Outlined Button
<div class="mt-4">
  <g-button color="primary" outlined>Outlined</g-button>
</div>

```vue
<g-button color="primary" outlined>Outlined</g-button>
```

### Disabled Button
<div class="mt-4">
  <g-button color="primary" disabled>Disabled</g-button>
</div>

```vue
<g-button color="primary" outlined>Outlined</g-button>
```

### As Link or Router Link
<div class="mt-4 mr-2 inline-flex">
  <g-button color="primary" href="https://google.de" target="_blank">As link</g-button>
</div>
<div class="mt-4 mr-2 inline-flex">
  <g-button color="secondary" to="/form-input">As router-link go to Input Fields</g-button>
</div>

```vue
<g-button color="primary" href="https://google.de" target="_blank">As link</g-button>
<g-button color="secondary" to="/form-input">As router-link go to Input Fields</g-button>
```

## Documentation

<[vuese](@/src/components/Button.vue)