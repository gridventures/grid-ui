# Dropdown <Badges version="0.0.5" update-version="none" />

## Examples
<g-dropdown>
  <g-button slot="trigger" color="secondary">Menu</g-button>

  <g-dropdown-item>Item 2</g-dropdown-item>
  <g-dropdown-item>Item 3</g-dropdown-item>
  <g-dropdown-divider />
  <g-dropdown-item icon="home">Icon</g-dropdown-item>
</g-dropdown>
<g-dropdown position="bottom-right">
  <g-button slot="trigger" outlined rounded="rounded-full">Ultra Long Trigger Text</g-button>

  <g-dropdown-item>Item 1</g-dropdown-item>
  <g-dropdown-item>Item 2</g-dropdown-item>
  <g-dropdown-item>Item 3</g-dropdown-item>
</g-dropdown>
<g-dropdown class="ml-4" position="bottom-right">
  <g-button slot="trigger">Small</g-button>

  <g-dropdown-item>Ultra Long Item Text</g-dropdown-item>
  <g-dropdown-item>Ultra Long Item Text</g-dropdown-item>
  <g-dropdown-item>Ultra Long Item Text</g-dropdown-item>
</g-dropdown>
<g-dropdown position="bottom-left" open-above>
  <g-button slot="trigger" color="dark">Open above</g-button>

  <g-dropdown-item>Ultra Long Item Text</g-dropdown-item>
  <g-dropdown-item>Ultra Long Item Text</g-dropdown-item>
  <g-dropdown-item>Ultra Long Item Text</g-dropdown-item>
</g-dropdown>

```vue
<g-dropdown position="bottom-right">
  <g-button slot="trigger" color="secondary">Menu</g-button>

  <g-dropdown-item @click="onclick">Item 2</g-dropdown-item>
  <g-dropdown-item @click="onclick">Item 3</g-dropdown-item>
  <g-dropdown-divider />
  <g-dropdown-item icon="home" @click="onclick">Icon</g-dropdown-item>
</g-dropdown>
```

## Documentation
<[vuese](@/src/components/Dropdown/Dropdown.vue)

## Dropdown-Item

### Examples
<g-dropdown-item>Dropdown Item</g-dropdown-item>

```vue
<g-dropdown-item @click="onclick">Dropdown Item</g-dropdown-item>
```

### Documentation
<[vuese](@/src/components/Dropdown/DropdownItem.vue)