---
sidebar: auto
---

# Getting started
Grid-UI is a ui-framework mainly powered by [tailwindcss](https://tailwindcss.com).
It uses the classes of tailwind and uses a modified color config uppon the base options from
tailwindcss following the concept from [Refactoring UI](https://refactoringui.com/book/).

But don't talk to much. Let's start!
## TailwindCSS Installation
Before you can use Grid-UI you have to setup a `tailwind.config.js` file. For more informations
please get a look into the nice [documentation of tailwind](https://tailwindcss.com/docs/installation).

## Grid-UI Installation
If you want to use all components this will be the safest way to implement them.
```bash
$ npm install @gridventures/grid-ui --save
```
Vue plugin initialization:
```js
import Vue from 'vue';
import GridUi from '@gridventures/grid-ui';
...
Vue.use(GridUi);
```
### Plugin Options
You can also pass optional parameters to the initialization.
```js
Vue.use(GridUi, {
  prefix: 'g',
});
```
### Single Component Installation
You can also import each component by their own.
```js
import Vue from 'vue';
import { Input, Button } from '@gridventures/grid-ui';
...
Vue.component('g-input', Input);
Vue.component('g-button', Button);
```

<g-button to="/theming">Let's config your theme.</g-button>

