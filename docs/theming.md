---
sidebar: auto
---

# Theming in Grid-UI <Badges version="0.0.5" update-version="none" />

## Getting started
Before you start you have to import Grid-UIs and materialdesignicons stylesheet

> With this imports you have to import the tailwind styles by your own.

```js
import '@mdi/font/css/materialdesignicons.min.css';
import '@gridventures/grid-ui/styles.scss';
```

Or if you want to import the tailwind styles and materialdesignicons within the grid-ui-styles:
```js
import '@gridventures/grid-ui/src/styles.scss';
```
This stylesheet contains the tailwind and materialdesignicons imports:
```scss
@import '@mdi/font/css/materialdesignicons.min.css';
@import 'tailwindcss/base';
@import 'tailwindcss/components';
@import 'tailwindcss/utilities';
```

## Default Grid-UI Settings
First you have to import the default theme settings from grid-ui into your `tailwind.config.js`. Otherwise the components cannot work as expected.
Grid-UI uses by default and in the examples the [Inter font family](https://rsms.me/inter/), a nice and beautiful open source ui font. If you want to use it as well
do the following:

```bash
$ npm i typeface-inter --save
```

`main.js`
```js
import 'typeface-inter';
import '@mdi/font/css/materialdesignicons.min.css'; // from previous step
import '@gridventures/grid-ui/styles.scss'; // from previous step
```

Of course you can use any font-family you want to by defining it in `theme.fontFamily` or you can still use the tailwind standard.

`tailwind.config.js`
```js
const defaultTheme = require('tailwindcss/defaultTheme');
const gridUiDefaults = require('@gridventures/grid-ui/src/utils/gridUiDefaults');
...

module.exports = {
  theme: {
    backgroundColor: theme => theme('colors'),
    borderColor: theme => ({
      ...theme('colors'),
      default: theme('colors.neutral.300', 'rgba(51, 59, 64, 0.1)'),
      // your custom settings should be here
    }),
    borderRadius: {
      ...defaultTheme.borderRadius,
      ...gridUiDefaults.borderRadius,
      // your custom settings should be here
    },
    boxShadow: {
      ...defaultTheme.boxShadow,
      ...gridUiDefaults.boxShadow,
      // your custom settings should be here
    },
    fontFamily: {
      ...defaultTheme.fontFamily,
      ...gridUiDefaults.fontFamily, // only if you want to use Inter font family
      // your custom settings should be here
    },
    inset: {
      ...defaultTheme.inset,
      ...gridUiDefaults.inset,
      // your custom settings should be here
    },
    minWidth: {
      ...defaultTheme.minWidth,
      ...gridUiDefaults.minWidth,
      // your custom settings should be here
    },
    maxHeight: {
      ...defaultTheme.maxHeight,
      ...gridUiDefaults.maxHeight,
      // your custom settings should be here
    },
  },
  variants: {
    ...gridUiDefaults.variants,
  },
  ...
};
```

## Theme Configuration

> Get a look into the [tailwind docs](https://tailwindcss.com/docs/customizing-colors) on how to individualize your colors.

Grid-UI follows the basics of tailwind but uses an own kind of color structure. In case of just using
color expressions for each color Grid-UI is strictly dividing your palette into a primary, a secondary, neutral,
black, white and state colors (success, warning, error).

Each color does have variants from '100' to '900' in hundred steps, a '050' as well as a `default` variant.
Default is by default the `500` variant of the color.
Also each color contains a `text-on-bg` variant which contains a light or dark font color for usage on a `color.500` background.
We highly recommend the chapter "Working with Color" on page 118 in [Refactoring UI](https://refactoringui.com/book/)
for building up your individual color palette.

### Use our palette generator

We deliver a built-in palette generator based on `chroma-js`. With this generator you can generate your palette by providing for each color only a default `hex` value.

```bash
$ npm install chroma-js --save
```

After that you can add following lines of code to your `tailwind.config.js`.

```js
...
const createColorSet = require('@gridventures/grid-ui/src/utils/createTailwindColorSet');
...
const colorSet = {
  black: '#000',
  white: '#FFF',
  primary: '#2680C2',
  secondary: '#F0B429',
  neutral: '#627D98', // a neutral tone maybe a gray tone
  alert: '#F56565',
  warning: '#ED8936',
  success: '#48BB78',
};
...
module.exports = {
  theme: {
    colors: {
      ...createColorSet.createPalette(colorSet).palette,
      // here you can add more colors if you want to
    },
    boxShadow: {
      ...defaultTheme.boxShadow, // from previous step
      ...gridUiDefaults.boxShadow, // from previous step
      ...createColorSet.createPalette(colorSet).shadows,
      // here you can add more shadows if you want to
    },
    ...
  },
};
```

Grid-UI builds his own `light` and `dark` tone. Dark is `neutral-900` and light is `neutral-050`.

### Build your own palette

Of course you can build your complete own palette. Please consider that you have to build a complete palette in which every color
except the `black` and `white` tone has to contain all gradations from `050` to `900` and `text-on-bg`. Also you have to build your own
`light`, `dark` and `transparent` tone as well as your own `outline-shadows` for each color.

An example for a full color palette (output of palette-generator):
```js
{
  transparent: 'transparent',
  black: '#000',
  white: '#fff',
  dark: {
    default: '#0b0e11',
    'text-on-bg': '#fff'
  },
  light: {
    default: '#dfe5eb',
    'text-on-bg': '#0b0e11'
  },
  primary: {
    '050': '#eaf6ff',
    '100': '#d7edfd',
    '200': '#b2dbf8',
    '300': '#97ccf1',
    '400': '#84c0eb',
    '500': '#2680C2',
    '600': '#2190e0',
    '700': '#0e619d',
    '800': '#07436e',
    '900': '#043152',
    default: '#2680C2',
    'text-on-bg': '#fff'
  },
  secondary: {
    '050': '#fffcf6',
    '100': '#fffaef',
    '200': '#fef5df',
    '300': '#fdf0d3',
    '400': '#fcedca',
    '500': '#F0B429',
    '600': '#f6c656',
    '700': '#d79807',
    '800': '#946803',
    '900': '#6d4d02',
    default: '#F0B429',
    'text-on-bg': '#0b0e11'
  },
  neutral: {
    '100': '#c2cdd8',
    '200': '#869cb1',
    '300': '#5a748d',
    '400': '#435669',
    '500': '#627D98',
    '600': '#212a33',
    '700': '#161c22',
    '800': '#0f1317',
    '900': '#0b0e11',
    '050': '#dfe5eb',
    default: '#627D98',
    'text-on-bg': '#fff'
  },
  alert: {
    '050': '#fff7f7',
    '100': '#fff0f0',
    '200': '#fee1e1',
    '300': '#fdd6d6',
    '400': '#fccece',
    '500': '#F56565',
    '600': '#f75959',
    '700': '#da0707',
    '800': '#950303',
    '900': '#6f0202',
    default: '#F56565',
    'text-on-bg': '#fff'
  },
  warning: {
    '050': '#fff9f5',
    '100': '#fff4eb',
    '200': '#fde9d8',
    '300': '#fce0c9',
    '400': '#fad9be',
    '500': '#ED8936',
    '600': '#f4984e',
    '700': '#ce6109',
    '800': '#8e4204',
    '900': '#693002',
    default: '#ED8936',
    'text-on-bg': '#fff'
  },
  success: {
    '050': '#dcfeea',
    '100': '#bffad8',
    '200': '#88ecb2',
    '300': '#63db95',
    '400': '#4bc97f',
    '500': '#48BB78',
    '600': '#248c4f',
    '700': '#106534',
    '800': '#074822',
    '900': '#043719',
    default: '#48BB78',
    'text-on-bg': '#fff',
  },
},
```

An example for a full shadow set:

```js
{
  'outline-dark': '0 0 0 3px rgba(11,14,17,0.5)',
  'outline-light': '0 0 0 3px rgba(223,229,235,0.5)',
  'outline-primary': '0 0 0 3px rgba(38,128,194,0.5)',
  'outline-secondary': '0 0 0 3px rgba(240,180,41,0.5)',
  'outline-neutral': '0 0 0 3px rgba(98,125,152,0.5)',
  'outline-alert': '0 0 0 3px rgba(245,101,101,0.5)',
  'outline-warning': '0 0 0 3px rgba(237,137,54,0.5)',
  'outline-success': '0 0 0 3px rgba(72,187,120,0.5)',
},
```

## tailwind.config.js Example

```js
const defaultTheme = require('tailwindcss/defaultTheme');
const gridUiDefaults = require('@gridventures/grid-ui/src/utils/gridUiDefaults');
const createColorSet = require('@gridventures/grid-ui/src/utils/createTailwindColorSet');

const colorSet = {
  black: '#000',
  white: '#FFF',
  primary: '#2680C2',
  secondary: '#F0B429',
  neutral: '#627D98', // a neutral tone maybe a gray tone
  alert: '#F56565',
  warning: '#ED8936',
  success: '#48BB78',
};

// tailwind.config.js
module.exports = {
  theme: {
    colors: {
      ...createColorSet.createPalette(colorSet).palette,
    },
    backgroundColor: theme => theme('colors'),
    borderColor: theme => ({
      ...theme('colors'),
      default: theme('colors.medium.300', 'rgba(51, 59, 64, 0.1)'),
    }),
    borderRadius: {
      ...defaultTheme.borderRadius,
      ...gridUiDefaults.borderRadius,
    },
    boxShadow: {
      ...defaultTheme.boxShadow,
      ...gridUiDefaults.boxShadow,
      ...createColorSet.createPalette(colorSet).shadows,
    },
    fontFamily: {
      ...defaultTheme.fontFamily,
      ...gridUiDefaults.fontFamily,
    },
    inset: {
      ...defaultTheme.inset,
      ...gridUiDefaults.inset,
    },
    minWidth: {
      ...defaultTheme.minWidth,
      ...gridUiDefaults.minWidth,
    },
    maxHeight: {
      ...defaultTheme.maxHeight,
      ...gridUiDefaults.maxHeight,
    },
  },
  variants: {
     ...gridUiDefaults.variants,
  },
  plugins: [],
};

```

<g-button to="/component/button">Let's start using grid-ui</g-button>