---
sidebar: auto
---

<style>
ul { list-style: disc }
</style>

# Roadmap
We have planned to build a huge ui framework with a lot of components and utilities.
We are right now working on version `0.x`. Our plan is it to release version `1.x` until
end of 2019. As we are right now in development there can be **breaking changes** from 
version `0.x` to `1.x` but we want to have as few breaking changes as possible.

## 0.x <Badge text="WIP" type="warn" />

### Components
- Form Elements:
  - Input <Badge text="DONE" />
  - Autocomplete <Badge text="DONE" />
  - Datepicker <Badge text="DONE" />
  - Checkbox <Badge text="DONE" />
  - Switch <Badge text="DONE" />
  - Radio <Badge text="DONE" />
  - Select <Badge text="DONE" />
  - Textarea <Badge text="DONE" />
  - File Input
  - Tag Input
- Button <Badge text="DONE" />
- Dropdown <Badge text="DONE" />
- Accordion <Badge text="WIP" type="warn" />
- Tag <Badge text="WIP" type="warn" />
- List <Badge text="WIP" type="warn" />
- Card <Badge text="WIP" type="warn" />
- Backdrop
- Sidepanel
- Modal
- FAB
- Data Table / Data List
- Toast
- Snackbar
- Message
- Toolbar

### Utilities
- Color Palette Generator <Badge text="DONE" />
- Grid UI Default Styles <Badge text="DONE" />
- Tailwind Plugins
  - `transition`
  - `font-variant-numeric`
  - `moz-focusring`

## 1.x
- Release source code on github

### Components
- Action Sheet
- Breadcrumbs
- Form Elements:
  - Slider / Range Slider
  - Number Input
  - Multi Select
  - Rating Indicator
  - Stepper
- Tabs
- Tooltip
- Loading
- Intro Tutorial
- Progress
- Pagination
- Dialogs
- Social Login Buttons

### Utilities
- `Not planned yet`