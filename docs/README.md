---
home: true
tagline: Flexible Vue UI Framework based on tailwindcss.
actionText: Get Started →
actionLink: /guide/
# features:
# - title: Simplicity First
#   details: Minimal setup to helps you focus on writing.
# - title: Customisable
#   details: All templates are customisables
# - title: Easy to particpate
#   details: Check the contributing guide and you dream <3 
footer: GPL-3.0-only | Copyright © 2019 - Gridventures GmbH
---
<div class="text-center mb-6">
  <a class="bg-alert text-white px-5 py-3 text-xl rounded transition hover:shadow-outline-alert" href="mailto:opensource@gridventures.de" target="_blank" color="alert" large>Report a bug</a>
</div>

<current-version />

::: warning
@gridventures/grid-ui is currently in beta mode and could be unstable.

Stable 1.0 release is coming until the end of 2019.
:::

::: danger
@gridventures/grid-ui is currently only tested and running in Chrome, Safari, Firefox 68+, Edge Chromium and Edge 16+.
IE11 support is maybe coming later.
:::