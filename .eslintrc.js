module.exports = {
  root: true,

  env: {
    browser: true,
    es6: true,
    node: true
  },

  parserOptions: {
    parser: 'babel-eslint',
  },

  extends: [
    // 'eslint:recommended',
    'airbnb-base',
    'plugin:vue/recommended',
  ],

  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'import/prefer-default-export': 'off',
    'class-methods-use-this': 'off',
    'no-unused-vars': 'warn',
    'no-param-reassign': 0,
    'array-callback-return': 0,
    'vue/no-side-effects-in-computed-properties': 0,
    'arrow-body-style': 'off',
    'vue/component-name-in-template-casing': ['error', 'kebab-case',
      {
        ignores: [],
      },
    ],
    'vue/singleline-html-element-content-newline': 0,
    'no-underscore-dangle': 0,
    'no-shadow': 0,
    'vue/max-attributes-per-line': [2, {
      singleline: 3,
      multiline: {
        max: 1,
        allowFirstLine: false,
      },
    }],
    'object-curly-newline': 0,
    'max-len': 'warn',
    'import/no-extraneous-dependencies': 0,
  },
};
