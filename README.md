# @gridventures/grid-ui
Grid-UI uses tailwindcss. So your projects needs a valid tailwind.config.js and/or a valid tailwindcss import.
[See tailwindcss docs for more informations!](https://tailwindcss.com/docs/installation)

## Documentation

[Have a look into our documentation to start with Grid-UI](https://beta.gridui.gridventures.de)

## [Roadmap](https://beta.gridui.gridventures.de/roadmap.html)
