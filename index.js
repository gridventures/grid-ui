/* eslint-disable import/order */
// Components
import { Portal, PortalTarget, MountingPortal } from 'portal-vue';
import Button from './src/components/Button.vue';
import Input from './src/components/Form/Input.vue';
import Autocomplete from './src/components/Form/Autocomplete.vue';
import Datepicker from './src/components/Form/Datepicker.vue';
import Checkbox from './src/components/Form/Checkbox.vue';
import Switch from './src/components/Form/Switch.vue';
import Icon from './src/components/Icon.vue';
import Sidebar from './src/components/Navigation/Sidebar.vue';
import SidebarItem from './src/components/Navigation/SidebarItem.vue';
import Select from './src/components/Form/Select.vue';
import Textarea from './src/components/Form/Textarea.vue';
import Radio from './src/components/Form/Radio.vue';
import Dropdown from './src/components/Dropdown/Dropdown.vue';
import DropdownItem from './src/components/Dropdown/DropdownItem.vue';
import DropdownDivider from './src/components/Dropdown/DropdownDivider.vue';

const defaultOptions = {
  prefix: 'g',
};

const GridUi = {
  install(Vue, options = defaultOptions) {
    const opt = { ...defaultOptions, ...options };
    Vue.component(`${opt.prefix}-portal`, Portal);
    Vue.component(`${opt.prefix}-mounting-portal`, MountingPortal);
    Vue.component(`${opt.prefix}-target`, PortalTarget);
    Vue.component(`${opt.prefix}-button`, Button);
    Vue.component(`${opt.prefix}-input`, Input);
    Vue.component(`${opt.prefix}-autocomplete`, Autocomplete);
    Vue.component(`${opt.prefix}-datepicker`, Datepicker);
    Vue.component(`${opt.prefix}-textarea`, Textarea);
    Vue.component(`${opt.prefix}-select`, Select);
    Vue.component(`${opt.prefix}-checkbox`, Checkbox);
    Vue.component(`${opt.prefix}-switch`, Switch);
    Vue.component(`${opt.prefix}-radio`, Radio);
    Vue.component(`${opt.prefix}-icon`, Icon);
    Vue.component(`${opt.prefix}-sidebar`, Sidebar);
    Vue.component(`${opt.prefix}-sidebar-item`, SidebarItem);
    Vue.component(`${opt.prefix}-dropdown`, Dropdown);
    Vue.component(`${opt.prefix}-dropdown-item`, DropdownItem);
    Vue.component(`${opt.prefix}-dropdown-divider`, DropdownDivider);
  },
};

export {
  Button,
  Input,
  Autocomplete,
  Datepicker,
  Checkbox,
  Switch,
  Icon,
  Sidebar,
  SidebarItem,
  Select,
  Textarea,
  Radio,
  Dropdown,
  DropdownItem,
  DropdownDivider,
};

export default GridUi;
