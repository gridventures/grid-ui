/* eslint-disable quote-props */
const defaultTheme = require('tailwindcss/defaultTheme');
const createColorSet = require('./src/utils/createTailwindColorSet');
const gridUiDefaults = require('./src/utils/gridUiDefaults');

const colorSet = {
  background: '#EFF1F5',
  black: '#000',
  white: '#fff',
  primary: '#2680C2',
  secondary: '#F0B429',
  neutral: '#627D98',
  alert: '#F56565',
  warning: '#ED8936',
  success: '#48BB78',
};

module.exports = {
  prefix: '',
  important: true,
  separator: ':',
  theme: {
    colors: {
      ...createColorSet.createPalette(colorSet).palette,
    },
    backgroundColor: theme => theme('colors'),
    borderColor: theme => ({
      ...theme('colors'),
      default: theme('colors.neutral.300', 'rgba(51, 59, 64, 0.1)'),
    }),
    borderRadius: {
      ...defaultTheme.borderRadius,
      ...gridUiDefaults.borderRadius,
    },
    boxShadow: {
      ...defaultTheme.boxShadow,
      ...gridUiDefaults.boxShadow,
      ...createColorSet.createPalette(colorSet).shadows,
    },
    fontFamily: {
      ...defaultTheme.fontFamily,
      ...gridUiDefaults.fontFamily,
      'mono': ['SFMono-Regular', 'Menlo'],
    },
    inset: {
      ...defaultTheme.inset,
      ...gridUiDefaults.inset,
    },
    minWidth: {
      ...defaultTheme.minWidth,
      ...gridUiDefaults.minWidth,
    },
    maxHeight: {
      ...defaultTheme.maxHeight,
      ...gridUiDefaults.maxHeight,
    },
  },
  variants: {
    ...gridUiDefaults.variants,
  },
  corePlugins: {},
  plugins: [],
};
