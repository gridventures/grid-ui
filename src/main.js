/* eslint-disable import/no-extraneous-dependencies */
import Vue from 'vue';
import GridUi from '../index';
import App from './App.vue';
import router from './router';

import Container from './helper/Container.vue';

import 'typeface-inter';
import './styles.scss';

Vue.use(GridUi);
Vue.component('container', Container);
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
