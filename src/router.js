import Vue from 'vue';
import Router from 'vue-router';
import FormInput from './views/FormInput.vue';
import FormAutocomplete from './views/FormAutocomplete.vue';
import FormDatepicker from './views/FormDatepicker.vue';
import FormCheckbox from './views/FormCheckbox.vue';
import FormSwitch from './views/FormSwitch.vue';
import FormSelect from './views/FormSelect.vue';
import FormTextarea from './views/FormTextarea.vue';
import FormRadio from './views/FormRadio.vue';
import Button from './views/Button.vue';
import Dropdown from './views/Dropdown.vue';

Vue.use(Router);

export const routes = [
  {
    path: '/form-input',
    component: FormInput,
  },
  {
    path: '/form-autocomplete',
    component: FormAutocomplete,
  },
  {
    path: '/form-datepicker',
    component: FormDatepicker,
  },
  {
    path: '/form-checkbox',
    component: FormCheckbox,
  },
  {
    path: '/form-switch',
    component: FormSwitch,
  },
  {
    path: '/form-radio',
    component: FormRadio,
  },
  {
    path: '/select',
    component: FormSelect,
  },
  {
    path: '/form-textarea',
    component: FormTextarea,
  },
  {
    path: '/button',
    component: Button,
  },
  {
    path: '/dropdown',
    component: Dropdown,
  },
];

export default new Router({
  routes: [
    ...routes,
    {
      path: '/',
      redirect: '/form-input',
    },
  ],
});
