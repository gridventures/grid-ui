module.exports = {
  backgroundColor: ['responsive', 'active', 'hover', 'focus'],
  borderColor: ['responsive', 'active', 'hover', 'focus'],
  padding: ['responsive', 'hover', 'focus'],
};
