const boxShadow = require('./defaults/boxShadow');
const fontFamily = require('./defaults/fontFamily');
const inset = require('./defaults/inset');
const minWidth = require('./defaults/minWidth');
const maxHeight = require('./defaults/maxHeight');
const borderRadius = require('./defaults/borderRadius');

const variants = require('./defaults/variants');

module.exports = {
  boxShadow,
  fontFamily,
  inset,
  minWidth,
  maxHeight,
  borderRadius,
  variants,
};
