const chroma = require('chroma-js');

const defaultOptions = {
  // useCssVars: false,
};

const defaultSet = {
  background: '#EFF1F5',
  black: '#000',
  white: '#fff',
  primary: '#2680C2',
  secondary: '#F0B429',
  neutral: '#627D98',
  alert: '#F56565',
  warning: '#ED8936',
  success: '#48BB78',
};

const relatives = {
  50: 0.87,
  100: 0.75,
  200: 0.5,
  300: 0.3,
  400: 0.15,
  600: 0.25,
  700: 0.5,
  800: 0.66,
  900: 0.75,
};

const palette = {
  transparent: 'transparent',
  black: null,
  white: null,
  dark: null,
  light: null,
  primary: {},
  secondary: {},
  neutral: {},
  alert: {},
  warning: {},
  success: {},
};

const shadows = {
  'outline-dark': null,
  'outline-light': null,
  'outline-primary': null,
  'outline-secondary': null,
  'outline-neutral': null,
  'outline-alert': null,
  'outline-warning': null,
  'outline-success': null,
};

const getHSLValue = (h, s, l, sDiff, lDiff, gradation, key) => {
  const relGrad = relatives[gradation];
  let modus = 'negative';
  if (Number(gradation) < 500) modus = 'positive';
  let newS;
  if (key !== 'neutral') {
    newS = s + (relGrad * sDiff);
  } else {
    newS = s;
  }
  let newL;
  if (modus === 'negative') {
    newL = l - (relGrad * l);
  } else if (modus === 'positive') {
    newL = l + (relGrad * lDiff);
  }
  const hex = chroma(`hsl(${h}, ${newS * 100}%, ${newL * 100}%)`).hex();
  return hex;
};

// const setCssVars = () => {
//   Object.keys(palette).map((color) => {
//     if (color !== 'black' && color !== 'white' && color !== 'transparent' && color !== 'light' && color !== 'dark') {
//       Object.keys(palette[color]).map((gradation) => {
//         if (gradation !== 'default') {
//           palette[color][gradation] = `var(--${color}-${gradation}, ${palette[color][gradation]})`;
//         } else {
//           palette[color] = `var(--${color}, ${palette[color][gradation]})`;
//         }
//       });
//     } else {
//       palette[color] = `var(--${color}, ${palette[color]})`;
//     }
//   });
// };

const getTextColor = (hex) => {
  return chroma(hex).get('lab.l') < 70 ? palette.white : palette.dark.default;
};

const getOutlineShadow = (hex) => {
  const shadowColor = chroma(hex).alpha(0.5).css();
  return `0 0 0 3px ${shadowColor}`;
};

const createPaletteFunc = (colorset = defaultSet, options = defaultOptions) => {
  // const opt = { ...defaultOptions, ...options };
  palette.black = colorset.black;
  palette.white = colorset.white;

  Object.keys(colorset).map((key) => {
    if (key !== 'black' && key !== 'white' && key !== 'dark' && key !== 'light' && key !== 'background') {
      const color = colorset[key];

      const hsl = chroma(color).hsl();
      const h = Number(hsl[0].toFixed(0));
      const s = Number(hsl[1].toFixed(2));
      const l = Number(hsl[1].toFixed(2));
      const sDiff = Number((1 - s).toFixed(2));
      const lDiff = Number((1 - l).toFixed(2));
      // [s, l] -> [y, x]
      palette[key]['050'] = getHSLValue(h, s, l, sDiff, lDiff, 50, key);
      palette[key][100] = getHSLValue(h, s, l, sDiff, lDiff, 100, key);
      palette[key][200] = getHSLValue(h, s, l, sDiff, lDiff, 200, key);
      palette[key][300] = getHSLValue(h, s, l, sDiff, lDiff, 300, key);
      palette[key][400] = getHSLValue(h, s, l, sDiff, lDiff, 400, key);
      palette[key][500] = color;
      palette[key].default = color;
      palette[key][600] = getHSLValue(h, s, l, sDiff, lDiff, 600, key);
      palette[key][700] = getHSLValue(h, s, l, sDiff, lDiff, 700, key);
      palette[key][800] = getHSLValue(h, s, l, sDiff, lDiff, 800, key);
      palette[key][900] = getHSLValue(h, s, l, sDiff, lDiff, 900, key);
      shadows[`outline-${key}`] = getOutlineShadow(color);
    }
  });

  // eslint-disable-next-line prefer-destructuring
  palette.dark = {
    default: palette.neutral[900],
    'text-on-bg': getTextColor(palette.neutral[900]),
  };
  palette.light = {
    default: palette.neutral['050'],
    'text-on-bg': getTextColor(palette.neutral['050']),
  };
  palette.background = {
    default: colorset.background,
    'text-on-bg': getTextColor(colorset.background),
  };
  shadows['outline-dark'] = getOutlineShadow(palette.dark.default);
  shadows['outline-light'] = getOutlineShadow(palette.light.default);

  Object.keys(colorset).map((key) => {
    if (key !== 'black' && key !== 'white' && key !== 'dark' && key !== 'light' && key !== 'background') {
      palette[key]['text-on-bg'] = getTextColor(palette[key].default);
    }
  });

  // if (opt.useCssVars) {
  //   setCssVars();
  // }

  return {
    palette,
    shadows,
  };
};

module.exports.createPalette = createPaletteFunc;
